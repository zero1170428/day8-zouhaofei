package com.afs.restapi.controller;


import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.CompanyService;
import com.afs.restapi.service.EmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/companies")
public class CompanyController {
    private CompanyService companyService;


    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Company createCompany(@RequestBody Company company) {
        return companyService.createCompany(company);
    }

    @GetMapping
    public List<Company> getCompanies() {
        return companyService.getCompanies();
    }

    @GetMapping("/{companyId}")
    public Company getCompanyByCompanyId(@PathVariable Long companyId) {
        return companyService.getCompanyByCompanyId(companyId);
    }

    @GetMapping("/{companyId}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable Long companyId){
        return companyService.getEmployeesByCompanyId(companyId);
    }

    @GetMapping(params = {"page","size"})
    public List<Company> getCompaniesByPageAndSize(@RequestParam Integer page,Integer size) {
        return companyService.getCompaniesByPageAndSize(page, size);
    }

    @PutMapping("/{companyId}")
    public Company updateCompanyName(@PathVariable Long companyId, @RequestBody Company company) {
        return companyService.updateCompanyName(companyId,company);
    }

    @DeleteMapping("/{companyId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Long companyId) {
        companyService.deleteCompany(companyId);

    }


}
