package com.afs.restapi.Exception;

public class NotFoundException extends RuntimeException {
    public NotFoundException() {
        super("employee id not found");
    }
}
