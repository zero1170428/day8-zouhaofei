package com.afs.restapi.Exception;

public class CompanyNotFoundException extends RuntimeException{
    public CompanyNotFoundException() {
        super("company id not found");
    }
}
