package com.afs.restapi.Exception;

public class EmployeeNotFoundException extends RuntimeException{
    public EmployeeNotFoundException(){
        super("employee id not found");
    }
}
