package com.afs.restapi.entity;

public class Employee {
    private Long id;
    private String name;
    private Integer age;
    private String gender;
    private Double salary;

    public Employee(Long id, String name, Integer age, String gender, Double salary) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.salary = salary;
    }

    private Long companyId;


    public Long getCompanyId() {
        return companyId;
    }


    private Boolean active;


    public void merge(Employee employee) {
        this.age = employee.age;
        this.salary = employee.salary;
    }

    public boolean isAgeValid(){
        return this.age >= 18;
    }

    public boolean isSlalaryMathAge(){
        if (this.age > 30 && this.salary < 20000){
            return false;
        }
        return true;
    }

    public boolean isActive(){
        return this.active;
    }

    public Employee() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
