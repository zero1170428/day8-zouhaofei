package com.afs.restapi.repository;

import com.afs.restapi.Exception.CompanyNotFoundException;
import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;


@Component
public class CompanyRepository {
    private static final List<Company> companies = new ArrayList<>();
    private static final AtomicLong atomicId = new AtomicLong(0);
    @Autowired
    private EmployeeRepository employeeRepository;

    public Company createCompany(Company company) {
        company.setCompanyId(atomicId.incrementAndGet());
        companies.add(company);
        return company;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public Company getCompanyByCompanyId(Long companyId) {
        return companies.stream().filter(company -> company.getCompanyId().equals(companyId)).findFirst().get();
    }

    public List<Employee> getEmployeesByCompanyId( Long companyId) {
        return employeeRepository.getEmployeesByCompanyId(companyId);
    }

    public List<Company> getCompaniesByPageAndSize(Integer page, Integer size) {
        return companies.stream()
                .skip((page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Company updateCompanyName( Company findCompany) {
        companies.removeIf(company -> Objects.equals(company.getCompanyId(),findCompany.getCompanyId()));
        companies.add(findCompany);
        return findCompany;
    }

    public void deleteCompany(Long companyId) {
        Company company = companies.stream()
                .filter(employeeTemp -> employeeTemp.getCompanyId().equals(companyId))
                .findFirst().get();
        companies.remove(company);
    }

    public void clearAll() {
        companies.clear();
    }
}



