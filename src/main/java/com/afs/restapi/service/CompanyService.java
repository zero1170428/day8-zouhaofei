package com.afs.restapi.service;

import com.afs.restapi.Exception.CompanyNotFoundException;
import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CompanyService {

    private CompanyRepository companyRepository;

    CompanyService(CompanyRepository companyRepository){
        this.companyRepository = companyRepository;
    }

    public  void clearAll() {
        companyRepository.clearAll();
    }

    public Company createCompany(Company company) {
        return companyRepository.createCompany(company);
    }

    public List<Company> getCompanies() {
        return companyRepository.getCompanies();
    }

    public Company getCompanyByCompanyId(Long companyId) {
        return companyRepository.getCompanyByCompanyId(companyId);
    }

    public List<Employee> getEmployeesByCompanyId( Long companyId) {
        return companyRepository.getEmployeesByCompanyId(companyId);
    }

    public List<Company> getCompaniesByPageAndSize(Integer page, Integer size) {
        return companyRepository.getCompaniesByPageAndSize(page, size);
    }

    public Company updateCompanyName(Long companyId, Company company) {
        Company findCompany = companyRepository.getCompanyByCompanyId(companyId);
        if (Objects.nonNull(findCompany)){
            findCompany.setName(company.getName());
            return companyRepository.updateCompanyName(findCompany);
        }
        throw new CompanyNotFoundException();
    }

    public void deleteCompany(Long companyId) {
        Company findCompany = companyRepository.getCompanyByCompanyId(companyId);
        if (Objects.nonNull(findCompany)){
            companyRepository.deleteCompany(companyId);
            return ;
        }
        throw new CompanyNotFoundException();
    }
}
