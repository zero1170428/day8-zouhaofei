package com.afs.restapi.service;

import com.afs.restapi.Exception.EmployeeAlreadyLeftTheCompanyException;
import com.afs.restapi.Exception.EmployeeNotFoundException;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.Exception.SalaryNotMatchAgeException;
import com.afs.restapi.Exception.NotFoundException;
import com.sun.tools.attach.AgentInitializationException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class EmployeeService {

    private EmployeeRepository employeeRepository;


    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public Employee findById(Long id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> findByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        return employeeRepository.findByPage(page, size);
    }

    public Employee insert(Employee newEmployee) {
        newEmployee.setActive(true);
        return employeeRepository.insert(newEmployee);
    }

    public Employee update(Long id, Employee employee) throws Exception {
        Employee findEmployee = employeeRepository.findById(id);
        if (Objects.nonNull(findEmployee)){
            if (findEmployee.getActive()){
                findEmployee.setAge(employee.getAge());
                findEmployee.setSalary(employee.getSalary());
                return employeeRepository.update(findEmployee);
            }
            throw new EmployeeAlreadyLeftTheCompanyException();

        }

        throw new EmployeeNotFoundException();
    }

    public void delete(Long id) {
        Employee toRemovedEmployee = employeeRepository.findById(id);
        if (Objects.nonNull(toRemovedEmployee)) {
            employeeRepository.delete(toRemovedEmployee);
            return ;
        }
        throw new EmployeeNotFoundException();

    }

    public void clearAll() {
        employeeRepository.clearAll();
    }


    public Employee create(Employee employee) throws AgentInitializationException {
        if(employee.isAgeValid()){
            if (employee.isSlalaryMathAge()){
                employee.setActive(true);
                return employeeRepository.insert(employee);
            }else{
                throw new SalaryNotMatchAgeException();
            }
        }
        throw new AgentInitializationException();

    }
}
