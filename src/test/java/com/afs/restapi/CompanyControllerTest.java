package com.afs.restapi;


import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.CompanyService;
import com.afs.restapi.service.EmployeeService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CompanyControllerTest {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    MockMvc client;

    @BeforeEach
    void cleanEmployeeData(){
        companyService.clearAll();
    }

    @Test
    void should_return_companies_when_getAllCompanies_given_companies() throws Exception {
        //given
        Company company= new Company(null, "oocl");
        companyService.createCompany(company);

        //when and then
        client.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(1)))
                .andExpect(jsonPath("$[0].companyId").value(company.getCompanyId()))
                .andExpect(jsonPath("$[0].name").value(company.getName()));
    }

    @Test
    void should_return_created_company_when_perform_createCompany_given_company_json() throws Exception {
        String ooclJson = new ObjectMapper().writeValueAsString(new Company(null,"oocl"));
        client.perform(MockMvcRequestBuilders.post("/companies")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ooclJson))
                .andExpect(jsonPath("$.name").value("oocl"));

    }

    @Test
    void should_return_company_when_perform_getCompanyByCompanyId_given_company_id() throws Exception {
        //given
        Company company= new Company(null, "oocl");
        companyService.createCompany(company);

        //when and then
        client.perform(MockMvcRequestBuilders.get("/companies/"+company.getCompanyId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.companyId").value(company.getCompanyId()))
                .andExpect(jsonPath("$.name").value(company.getName()));

    }

    @Test
    void should_return_employees_when_perform_getEmployeesByCompanyId_given_company_id() throws Exception {
        //given
        Employee john = new Employee(null, "John Smith", 32, "Male", 5000.0);
        john.setCompanyId(1l);
        employeeService.insert(john);
        //when and then
        client.perform(MockMvcRequestBuilders.get("/companies/1/employees"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(john.getId()))
                .andExpect(jsonPath("$[0].name").value(john.getName()))
                .andExpect(jsonPath("$[0].age").value(john.getAge()))
                .andExpect(jsonPath("$[0].gender").value(john.getGender()))
                .andExpect(jsonPath("$[0].salary").value(john.getSalary()))
                .andExpect(jsonPath("$[0].companyId").value(john.getCompanyId()));

    }

    @Test
    void should_return_companies_when_perform_getCompaniesByPageAndSize_given_page_and_size() throws Exception {
        //given
        Company company1= new Company(null, "oocl");
        Company company2= new Company(null, "cosco");
        companyService.createCompany(company1);
        companyService.createCompany(company2);
        //when and then
        client.perform(MockMvcRequestBuilders.get("/companies")
                .param("page","1")
                .param("size","2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(2)))
                .andExpect(jsonPath("$[0].companyId").value(company1.getCompanyId()))
                .andExpect(jsonPath("$[0].name").value(company1.getName()))
                .andExpect(jsonPath("$[1].companyId").value(company2.getCompanyId()))
                .andExpect(jsonPath("$[1].name").value(company2.getName()));


    }

    @Test
    void should_return_void_when_perform_deleteCompany_given_company_id() throws Exception {
        Company company= new Company(null, "oocl");
        companyService.createCompany(company);
        client.perform(MockMvcRequestBuilders.delete("/companies/"+company.getCompanyId()))
                .andExpect(status().isNoContent());

    }

    @Test
    void should_return_company_when_perform_updateCompanyName_given_name() throws Exception {
        Company company= new Company(null, "oocl");
        companyService.createCompany(company);
        company.setName("cosco");
        String companyJson = new ObjectMapper().writeValueAsString(company);
        client.perform(MockMvcRequestBuilders.put("/companies/"+company.getCompanyId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(companyJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.companyId").value(company.getCompanyId()))
                .andExpect(jsonPath("$.name").value(company.getName()));
    }


}
