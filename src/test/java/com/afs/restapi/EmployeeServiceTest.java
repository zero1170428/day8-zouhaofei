package com.afs.restapi;

import com.afs.restapi.Exception.EmployeeAlreadyLeftTheCompanyException;
import com.afs.restapi.Exception.SalaryNotMatchAgeException;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import com.sun.tools.attach.AgentInitializationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;


public class EmployeeServiceTest {
    EmployeeRepository employeeRepositoryMock = mock(EmployeeRepository.class);

    @Test
    void should_not_create_successfully_when_create_given_age_is_invalid(){
        EmployeeService employeeService = new EmployeeService(employeeRepositoryMock);
        Employee employee = new Employee(null,"ff",15,"Male",15000d);
        Assertions.assertThrows(AgentInitializationException.class,()->employeeService.create(employee));
    }


    @Test
    void should_not_create_successfully_when_create_employee_given_age_is_over_30_and_salary_below_20000() {
        EmployeeService employeeService = new EmployeeService(employeeRepositoryMock);
        Employee employee = new Employee(1L, "mike", 33, "Female", 19999.9);
        Assertions.assertThrows(SalaryNotMatchAgeException.class, () -> employeeService.create(employee));
    }

    @Test
    void should_set_active_when_create_employee_given_an_employee() throws AgentInitializationException {
        EmployeeService employeeService = new EmployeeService(employeeRepositoryMock);
        Employee employee = new Employee(1L, "zero", 19, "Female", 8000.2);
        employeeService.create(employee);
        Assertions.assertEquals(employee.getActive(),true);

    }

    @Test
    void should_active_is_false_when_delete_employee_given_id() {
        EmployeeService employeeService = new EmployeeService(employeeRepositoryMock);
        Employee employee = new Employee(1L, "zero", 20, "Female", 8000.2);
        given(employeeRepositoryMock.findById(1L)).willReturn(employee);
        employeeService.delete(1l);
        verify(employeeRepositoryMock, times(1)).delete(employee);
    }

    @Test
    void should_not_update_successfully_when_update_employee_given_id_and_employee() {
        EmployeeService employeeService = new EmployeeService(employeeRepositoryMock);
        Employee employee = new Employee(1L, "mike", 33, "Female", 8000.2);
        employee.setActive(false);
        given(employeeRepositoryMock.findById(1L)).willReturn(employee);
        Assertions.assertThrows(EmployeeAlreadyLeftTheCompanyException.class, () -> employeeService.update(1L, employee));
    }


}
