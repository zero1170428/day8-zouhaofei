# Daily Report (2023/07/19)

## O

### Code review

##### Using `@ControllerAdvice` and `@ExceptionHandler`: Create a class with the `@ControllerAdvice` annotation, and define one or more methods in that class using the `@ExceptionHandler` annotation to handle specific exception types. These methods will capture the thrown exceptions and provide custom handling logic.

### CI/CD

##### This Friday will be a presentation of CI/CD. I am mainly responsible for the first part, introducing CI/CD. Today I checked some information about CI/CD, determined the direction of entry, and mainly talked about the development process of CI/CD.

## springboot

##### Today, I mainly write some corresponding test cases for the web development of springboot. The API test cases written in the morning are not too problematic. In the afternoon, there are many things I don’t understand about the test of the Service layer.

## R

##### meaningful

## I

##### I think this afternoon's class is a bit difficult, what impressed me the most is the various assertion syntax

## D

##### When writing server-side code, the corresponding test code will be written for each level